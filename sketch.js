/**
 __        __                            _       _     ____  _____ 
 \ \      / /_ ___   _______      ____ _| |_ ___| |__ | __ )| ____|
  \ \ /\ / / _` \ \ / / _ \ \ /\ / / _` | __/ __| '_ \|  _ \|  _|  
   \ V  V / (_| |\ V /  __/\ V  V / (_| | || (__| | | | |_) | |___ 
    \_/\_/ \__,_| \_/ \___| \_/\_/ \__,_|\__\___|_| |_|____/|_____|

 SoundCard Oscilloscope for Web browser

 Version 1.0
 
 TODO:
 - Stream Perfo display
 - Amplitude
 
*/

// Appendix - Calculus & Miscelaneous JS

function pf(x) {
  return Number.parseFloat(x).toPrecision(3);
}

// overloading modulo for accurately negative modulo calculation
Number.prototype.mod = function(n) {
	var m = (( this % n) + n) % n;
	return m < 0 ? m + Math.abs(n) : m;
};

// =====================================
// ======== Global Parameters - Change at your convenience
// =====================================
// Debug Info
var ddebug = false;			// Enable for on-screen debug data beyond mouse
// HMI Paramaters
var marginratio = 0.1;		// Screen Width / Sidebar control size ratio
var marginlimit=90;			// Maximum margin menu size limit
var text_size=12;			// Overall Text size
var scrollspeed=0.1;		// Menu scroll speed ratio towards width
// Signal Display
var haloing=20;				// Enables grey halo between signal and reference (0: No Halo, 255:white fill)
var sig_weight=2;			// Signal thickness display

// /!\ MODIFICATIONS BELOW THIS LINE ARE AT YOUR OWN RISK :) /!\

// =====================================
// ======== Variables init
// =====================================

//HMI
var leftmargin=30;
var rightmargin=30;
var splashclick = true; 
var splash;  // Splash Screen image
var dmode=0; //0 - Time; 1 - X/Y; 2 - FFT
var scrspd;  // Scroll Speed
var xmin,xmax;

// Measurement
var measure_state = 0; // No measurement ongoing
var m_points = [0,0,0,0];

// Channel Data Allocation 
var dhsize=1200;			// Display Size
var chA_y; 					// chA y position on screen, normalised with height
var chB_y; 					// chB y position on screen, normalised with height

var chA_active=true; 		// chA enable/disable
var chB_active=true; 		// chB enable/disable

var gainA = 8; 				// chA Gain Normalised
var gainB = 8; 				// chB Gain Normalised

var leftmenu=0;
var leftmenuactive=false;
var rightmenu=0;
var rightmenuactive=false;
var mainmenu=0;
var mainmenuactive=false;

var samplerate; 			// Variable to store audio environment samplerate

// RAW Arrays of amplitude values (development purpose)
var samplesA = [];
var samplesB = [];

// time-resolution-aligned Data Arrays for Triggered display in flip/flop fashion
var samplesA_r = [7200];
var samplesB_r = [7200];
var samplesA_b = [2400];
var samplesB_b = [2400];
var samples_trig=[];
var incA=0;
var incB=0;
var iA=0;
var iB=0;
var ia=0;
var ib=0;
var bout=true;

// Trigger Management
var trigged = false;
var trigpos = 0;
var trig_lvl = 0.001;
var trig_x = dhsize;
var tsource = 0; // 0:CHA, 1:CHB
var tmode = 0; // 0:Auto - 1:Single
var running =true; // false:Stopped, true:Ready,
var trig_set=false;
var trig_dir=false; // false : Rising edge, true : falling edge
var trig_last=false; // true if last buffer walkthrough had a Trig push

// Time Management
var time_res = 5;
var time_resolution = [0.000025,0.00005,0.0001,0.00025,0.0005,0.001,0.0025,0.005,0.01,0.025,0.05,0.1,0.2,0.5,1];
var time_resolution_str = ['25µs','50µs','100µs','250µs','500µs','1ms','2ms','5ms','10ms','20ms','50ms','100ms','200ms','500ms','1s'];

// Amplitude Management
var amp_res_A = 5;
var amp_res_B = 5;
var amp_resolution = [0.001,0.0025,0.005,0.01,0.025,0.05,0.1,0.25,0.5,1,2.5,5];
var amp_resolution_str = ['1mV','2.5mV','5mV','10mV','25mV','50mV','0.1V','0.25V','0.5V','1V','2.5V','5V'];
var cal_factor = 1.414; //Voltage corresponding to "1" float input value, will be overwritten by calibration. 1.414 reference is the voltage corresponding to "1" in float on a soundcard line input signal.


// =====================================
// ======== Audio Environment Setup
// =====================================
// Overloading "getUserMedia" to ensure cross-browser compatibility
navigator.getUserMedia = navigator.getUserMedia ||     
   navigator.webkitGetUserMedia ||     
   navigator.mozGetUserMedia ||
   navigator.mediaDevices.getUserMedia ||   
   navigator.msGetUserMedia; 

// Audio Acquisition init
navigator.getUserMedia({ audio: true },        
  function (e) {     
   // creates the audio context  
   window.AudioContext = window.AudioContext || window.webkitAudioContext;     
   context = new AudioContext(); 
   samplerate = context.sampleRate;   // Gather Samplerate for further time alignment of buffers.
   
   incA = samplerate*time_resolution[time_res]/20;
   incB = samplerate*time_resolution[time_res]/20;
   
   // creates an audio node from the microphone incoming stream     
   mediaStream = context.createMediaStreamSource(e);    
   // https://developer.mozilla.org/en-US/docs/Web/API/AudioContext/createScriptProcessor     
   
   var bufferSize = 2048; // Audio buffer size, TODO : evaluate several configurations to check CPU load on each browser.     
   var numberOfInputChannels = 2; // Dual Channel TODO : more?
   var numberOfOutputChannels = 2; // Outputs. Unused for now    
   
   // Creating the recorder:
   // Note : createJavaScriptNode is obsolete, but if your browser supports it instead of createScriptProcessor
   if (context.createScriptProcessor) {     
      recorder = context.createScriptProcessor(bufferSize, numberOfInputChannels, numberOfOutputChannels);     
   } else {     
      recorder = context.createJavaScriptNode(bufferSize, numberOfInputChannels, numberOfOutputChannels);     
   }    
   
   // Audio Buffer Processing init
   recorder.onaudioprocess = function (e) { 

      samplesA = new Float32Array(e.inputBuffer.getChannelData(0)); 
      samplesB = new Float32Array(e.inputBuffer.getChannelData(1)); 

	  bout=true;	  
	  while (bout) {
		samplesA_r[ia]=map(samplesA[int(iA)],-1,1,-cal_factor,cal_factor);
		samplesB_r[ia]=map(samplesB[int(iA)],-1,1,-cal_factor,cal_factor);
		iA+=incA; if (iA>=2048) {iA-=2048; bout=false;}
		ia=ia+1; 
		if (ia==7200) {ia=0; if (running) a_proc();}
		if (ia==2400) { if (running) a_proc(); }
		if (ia==4800) { if (running) a_proc();}
	  }	  
   }   
   
   // connect the recorder with the input stream     
   mediaStream.connect(recorder);     
   recorder.connect(context.destination);     
  },function (e) {// Audio Context Init Failure
	  console.log("No Audio Context"); 
	  alert("No Audio Context"); 
	  });

// =====================================
// ======== Main Functions
// =====================================		  

// Function called when time reference is changed for scaling	  
function time_update() {
   incA = samplerate*time_resolution[time_res]/20;
   incB = samplerate*time_resolution[time_res]/20;
}	

// Channel Amplitude resolution update
function amp_update() {
	gainA = 20/amp_resolution[amp_res_A];
	gainB = 20/amp_resolution[amp_res_B];
}

// Trig Conditions Update
function trig_update() {
	trig_x=int((mouseX-width/2)+1200);
	var chy; var gain; var amp_res_x;
	if (tsource==0) {chy=chA_y; gain=gainA; amp_res_x=amp_res_A;} else {chy=chB_y; gain=gainB; amp_res_x=amp_res_B;}
	//trig_lvl=(chy*height-mouseY)/(gain*height/2);
	trig_lvl=(chy*height-mouseY)*amp_resolution[amp_res_x]/20;
}

function trig_init() {
	trig_x=1200;
	trig_lvl=0.01;
}

// Audio Process called every time the one of the 3 display windows buffered is filled	  
function a_proc() {
		trigged=false;	
		switch(ia) {
			case 0:
				trig_last=false;
				for (var i=2400;i<4800;i+=1) {
					if (!trig_dir) {
					if ((tsource==0)&&(samplesA_r[i]>trig_lvl)&&(samplesA_r[i-1]<=trig_lvl)&&(!trigged)) {
						trigpos=i; trigged=true;
					}
					if ((tsource==1)&&(samplesB_r[i]>trig_lvl)&&(samplesB_r[i-1]<=trig_lvl)&&(!trigged)) {
						trigpos=i; trigged=true;
					}
					} else {
					if ((tsource==0)&&(samplesA_r[i]<trig_lvl)&&(samplesA_r[i-1]>=trig_lvl)&&(!trigged)) {
						trigpos=i; trigged=true;
					}
					if ((tsource==1)&&(samplesB_r[i]<trig_lvl)&&(samplesB_r[i-1]>=trig_lvl)&&(!trigged)) {
						trigpos=i; trigged=true;
					}	
					}
				}
				if(trigged){
					var j=0;
					trig_last=true;
					if( tmode==1) running=false;
					for(var i=trigpos-trig_x; i<trigpos-trig_x+2400; i+=1) {
						samplesA_b[j]=samplesA_r[i]; samplesB_b[j]=samplesB_r[i]; j+=1;
					}
				}
        break;
			case 2400:
				trig_last=false;
				for (var i=4800;i<7200;i+=1) {
					if (!trig_dir) {
					if ((tsource==0)&&(samplesA_r[i]>trig_lvl)&&(samplesA_r[i-1]<=trig_lvl)&&!trigged) {
						trigpos=i; trigged=true;
					}
					if ((tsource==1)&&(samplesB_r[i]>trig_lvl)&&(samplesB_r[i-1]<=trig_lvl)&&!trigged) {
						trigpos=i; trigged=true;
					}
					} else {	
					if ((tsource==0)&&(samplesA_r[i]<trig_lvl)&&(samplesA_r[i-1]>=trig_lvl)&&!trigged) {
						trigpos=i; trigged=true;
					}
					if ((tsource==1)&&(samplesB_r[i]<trig_lvl)&&(samplesB_r[i-1]>=trig_lvl)&&!trigged) {
						trigpos=i; trigged=true;
					}
					}
				}
				if(trigged){
					var j=0;
					trig_last=true;
					if( tmode==1) running=false;
					for(var i=trigpos-trig_x; i<trigpos-trig_x+2400; i+=1) {
						samplesA_b[j]=samplesA_r[i.mod(7200)]; samplesB_b[j]=samplesB_r[i.mod(7200)]; j+=1;
					}
				}		
		break;
			case 4800:
				trig_last=false;
				for (var i=7200;i<9600;i+=1) {
					if (!trig_dir) {
					if ((tsource==0)&&(samplesA_r[i.mod(7200)]>trig_lvl)&&(samplesA_r[i.mod(7200)-1]<=trig_lvl)&&!trigged) {
						trigpos=i; trigged=true;
					}
					if ((tsource==1)&&(samplesB_r[i.mod(7200)]>trig_lvl)&&(samplesB_r[i.mod(7200)-1]<=trig_lvl)&&!trigged) {
						trigpos=i; trigged=true;
					}	
					} else {
					if ((tsource==0)&&(samplesA_r[i.mod(7200)]<trig_lvl)&&(samplesA_r[i.mod(7200)-1]>=trig_lvl)&&!trigged) {
						trigpos=i; trigged=true;
					}
					if ((tsource==1)&&(samplesB_r[i.mod(7200)]<trig_lvl)&&(samplesB_r[i.mod(7200)-1]>=trig_lvl)&&!trigged) {
						trigpos=i; trigged=true;
					}
					}
				}
				if(trigged){
					var j=0;
					trig_last=true;
					if( tmode==1) running=false;
					for(var i=trigpos-trig_x; i<trigpos-trig_x+2400; i+=1) {
						samplesA_b[j]=samplesA_r[i.mod(7200)]; samplesB_b[j]=samplesB_r[i.mod(7200)]; j+=1;
					}
				}				
		break;
		} 
	
}

// =====================================
// ======== P5JS Environment
// =====================================	  

// P5JS Init function
function setup() {
  var cnv = createCanvas(windowWidth, windowHeight);
  noFill();
  stroke(240);
  
  for (var i = 0; i < 2400; i++){
	samplesB_b[i]=0;
	samplesA_b[i]=0;
  }
  for (var i = 0; i < 7200; i++){
	samplesB_r[i]=0;
	samplesA_r[i]=0;
  }
  
  leftmargin=width*marginratio;  if (leftmargin>marginlimit) leftmargin=marginlimit;
  rightmargin=width*marginratio; if (rightmargin>marginlimit) rightmargin=marginlimit;
  
  textSize(text_size);
  
  time_update();
  
  scrspd = scrollspeed*width;
  
  chA_y = 0.5;
  chB_y = 0.5;
  
  trig_level = 0.2; 
  
  chA_active=true;
  chB_active=true;
  
  xmin=width/2-dhsize;
  xmax=width/2+dhsize;
  
  trig_init();
  amp_update();
  
  splash = loadImage("inc/splash.png");
} 
  
// P5JS Draw function
function draw() {

  background(30, 30, 30, 220);

  var bufLen = samplesA.length;

  drawAxis();
  
  draw_left_menu();
  
  draw_right_menu();  
  
  draw_cursor();
  
  buffermon();  
  
  if (measure_state>0) draw_measure();
  
  // draw the samples
  if (dmode==0) {
  if(chB_active) display_raw_buffer(samplesB_b, 2400,chB_y,gainB,"lightblue");
  if(chA_active) display_raw_buffer(samplesA_b, 2400,chA_y,gainA,"lightgreen"); 
  }
  if (dmode==1) {
	  display_xy_buffer(samplesA_b,samplesB_b, 2400,"yellow");
  }
  if (splashclick) image(splash, width/2-512,leftmargin); else {tint(255,255,255,70); image(splash, leftmargin, height-leftmargin*3/5, leftmargin*2.56, leftmargin); }

  if(ddebug) labelStuff(bufLen);
  
}

// =====================================
// ======== Display Subfuncions
// =====================================

function draw_measure() {
	fill(200,160,0,60);
	switch(measure_state) {
			case 1: // Waiting for Measurement First Click
				stroke(255,165,0,200);
				ellipse(mouseX, mouseY, 20, 20);
				ellipse(mouseX, mouseY, 5, 5);
			break;
			case 2: // Waiting for Measurement Second Click
				stroke(255,165,0,200);
				ellipse(m_points[0], m_points[1], 20, 20);
				ellipse(m_points[0], m_points[1], 5, 5);
				line(mouseX,mouseY,m_points[0], m_points[1]);
				ellipse(mouseX, mouseY, 20, 20);
				ellipse(mouseX, mouseY, 5, 5);
			break;
			case 3: // Display measurement Data
				stroke(255,165,0,200);
				ellipse(m_points[0], m_points[1], 20, 20);
				ellipse(m_points[2], m_points[3], 20, 20);
				ellipse(m_points[0], m_points[1], 5, 5);
				ellipse(m_points[2], m_points[3], 5, 5);
				line(m_points[0], m_points[1],m_points[2], m_points[3]);
				textAlign(LEFT);
				// Time
				var deltat=(m_points[2]-m_points[0])*time_resolution[time_res]/20;
				var deltap=1/deltat;
				if (deltat>1) {
					deltat=pf(deltat);
				    text('‎[T] '+ deltat +' s', mouseX+20, mouseY);}
				if ((deltat>0.001)&&(deltat<1)) {
					deltat=pf(deltat*1000);
				    text('‎[T] '+ deltat +' ms', mouseX+20, mouseY);}
				if (deltat<0.001) {
					deltat=pf(deltat*1000000);
				    text('‎[T] '+ deltat +' us', mouseX+20, mouseY);}	
				var deltava=(m_points[3]-m_points[1])*amp_resolution[amp_res_A]/20;
				var deltavb=(m_points[3]-m_points[1])*amp_resolution[amp_res_B]/20;
				
				if (deltap<1000) text('‎[F] '+ pf(deltap) +' Hz', mouseX+20, mouseY+12); else text('‎[F] '+ pf(deltap/1000) +' kHz', mouseX+20, mouseY+12);
				if(chA_active) {stroke(0,255,0,200); text('[‎A] '+ pf(deltava) +' V', mouseX+20, mouseY+24);}
				if(chB_active) {stroke(0,0,255,200); text('‎[B] '+ pf(deltavb) +' V', mouseX+20, mouseY+36)}
				
			break;
	}
}

function draw_cursor() {
	if(trig_set) {
		var ldheight;
		if (tsource==0) ldheight=chA_y*height; else ldheight=chB_y*height; 
		fill(200,200,200,60);
		stroke(220,220,120,50);
		line(mouseX,mouseY,mouseX,ldheight);
		line(mouseX,mouseY,width/2,mouseY);
		stroke(255,255,255,50);
		ellipse(mouseX, mouseY, 20, 20);
		stroke(220,220,120);
		line(mouseX-10,mouseY,mouseX+10,mouseY);
		line(mouseX,mouseY-10,mouseX,mouseY+10);
	}
	stroke(220,220,120);
	var px; var py;
	px=trig_x-1200+width/2;
	var chy; var gain; var amp_res_x;
	if (tsource==0) {chy=chA_y; gain=gainA; amp_res_x=amp_res_A;} else {chy=chB_y; gain=gainB;amp_res_x=amp_res_A;}
	py=-1*trig_lvl*20/amp_resolution[amp_res_x]+chy*height;
	line(px-10,py,px+10,py);
	line(px,py+10,px,py-10);
}

function draw_right_menu() {
  strokeWeight(1.0); 
  stroke(50,50,155,255);
  fill(50,50,155,255);
  rect(width-rightmargin,height-rightmargin,width,height);
  fill(50,50,255,40);
  stroke(50,50,255,50);
  rect(width-rightmargin,0,width,height); 
  stroke(255,255,255,50);
  rect(width-rightmargin-rightmenu,height-rightmargin,width,height); 
  
  if (rightmenuactive) {
		if (rightmenu<5*rightmargin)rightmenu+=scrspd; else rightmenu=5*rightmargin;
	} else {
		if (rightmenu>0)rightmenu-=scrspd; else rightmenu=0;
	}
  
  // Signal Arrow and Baseline
  if(chB_active) {
	  var dstring;
  stroke(50,50,255,40);
  line(0,chB_y*height,width,chB_y*height);
  stroke("lightblue");
  noFill();
  strokeWeight(1.0);
  strokeJoin(ROUND);
  beginShape();
    vertex(width, chB_y*height-13);
    vertex(width-15, chB_y*height);
    vertex(width, chB_y*height+13);
  endShape();
  }
  
  // Main Menu
  stroke(200,200,200,60);
  fill(200,200,200,200);
  rect(width-rightmargin,0,width,rightmargin);   
  fill(200,200,200,60);
  rect(width-rightmargin-mainmenu,0,width,rightmargin); 
  if (mainmenuactive) {
		if (mainmenu<6*rightmargin)mainmenu+=scrspd; else mainmenu=6*rightmargin;
	} else {
		if (mainmenu>0)mainmenu-=scrspd; else mainmenu=0;
	}  
  stroke(20,20,20);
  fill(20,20,20);
  textAlign(CENTER);
  text("Menu",width-rightmargin/2,rightmargin/2+text_size/2);  
  
  if (mainmenu==6*rightmargin) {
		stroke(200,200,200,200);
		fill(200,200,200,60);
        rect(width-2*rightmargin,3,rightmargin-3,rightmargin-6);
		ellipse(width-2*rightmargin-2,rightmargin/2+3,rightmargin*3/7,rightmargin*1/3);
		rect(width-3*rightmargin,3,rightmargin-3,rightmargin-6);
		rect(width-4*rightmargin,3,rightmargin-3,rightmargin-6);		
		rect(width-5*rightmargin,3,rightmargin-3,rightmargin-6);
		rect(width-7*rightmargin,3,rightmargin-3,rightmargin-6);
		if(trig_set) fill(220,220,200,100); else fill(200,200,200,60);
		rect(width-6*rightmargin,3,rightmargin-3,rightmargin-6);
		stroke(255,255,255);
		textAlign(CENTER);
		text("Time+",width-rightmargin-rightmargin/2,rightmargin/2+text_size/2);
		text(time_resolution_str[time_res],width-2*rightmargin-2,rightmargin/2+text_size/2);
		text("Time-",width-2*rightmargin-rightmargin/2,rightmargin/2+text_size/2);
		if (tsource==0) dstring="ChA"; else dstring="ChB";
		text("Source\n"+dstring,width-3*rightmargin-rightmargin/2,rightmargin/2+text_size/2);
		if (tmode==0) dstring="Auto"; else dstring="Single";
		text("Mode\n"+dstring,width-4*rightmargin-rightmargin/2,rightmargin/2+text_size/2);
		if (trig_set) dstring="Set Trig\nlevel"; else dstring="Trig\nLevel";
		text(dstring,width-5*rightmargin-rightmargin/2,rightmargin/2+text_size/2);	
	    if (trig_dir) dstring="Falling\nEdge"; else dstring="Rising\nEdge";
		text(dstring,width-6*rightmargin-rightmargin/2,rightmargin/2+text_size/2);
  }
  
  // Text Labels
  stroke(0,0,50);
  fill(0,0,50);
  textAlign(CENTER);
  text("ChB\n"+amp_resolution_str[amp_res_B],width-rightmargin/2,height-rightmargin/2+text_size/2);  
  
  if (rightmenu==5*rightmargin) {
		fill(50,50,255,10);
		stroke(50,50,255,200);
        rect(width-2*rightmargin,height-rightmargin+3,rightmargin-3,rightmargin-6); 
		rect(width-3*rightmargin,height-rightmargin+3,rightmargin-3,rightmargin-6);
		rect(width-4*rightmargin,height-rightmargin+3,rightmargin-3,rightmargin-6);
		if(measure_state>0) fill(50,50,255,100); else fill(50,50,255,10);		
		rect(width-5*rightmargin,height-rightmargin+3,rightmargin-3,rightmargin-6);
		if(chB_active) fill(50,50,255,100); else fill(50,50,255,10);		
		rect(width-6*rightmargin,height-rightmargin+3,rightmargin-3,rightmargin-6);
		stroke(150,150,255);
		textAlign(CENTER);
		text("Amp-",width-rightmargin-rightmargin/2,height-rightmargin/2+text_size/2);
		text("Amp+",width-2*rightmargin-rightmargin/2,height-rightmargin/2+text_size/2);
		text("<>",width-3*rightmargin-rightmargin/2,height-rightmargin/2+text_size/2); // Spare for calibration
		text("Meas",width-4*rightmargin-rightmargin/2,height-rightmargin/2+text_size/2);
		if (chB_active) {dstring="On";stroke(150,150,255);} else {dstring="Off";stroke(50,50,255);}
		text(dstring,width-5*rightmargin-rightmargin/2,height-rightmargin/2+text_size/2);	
  }
}

function draw_left_menu() {
	var dstring;
	strokeWeight(1.0);  
	fill(50,150,50,255);
	stroke(50,150,50,255);
	rect(0,0,leftmargin,leftmargin);
	fill(50,255,50,40);
	stroke(50,255,50,50);
    rect(0,0,leftmargin,height);
	stroke(255,255,255,50);
	rect(0,0,leftmargin+leftmenu,leftmargin);
	
	if (leftmenuactive) {
		if (leftmenu<5*leftmargin)leftmenu+=scrspd; else leftmenu=5*leftmargin;
	} else {
		if (leftmenu>0)leftmenu-=scrspd; else leftmenu=0;
	}
	
	// Signal Arrow and Baseline
  if(chA_active) {
  stroke(50,255,50,40);
  line(0,chA_y*height,width,chA_y*height);
  stroke("lightgreen");
  noFill();
  strokeWeight(1.0);
  strokeJoin(ROUND);
  beginShape();
    vertex(0, chA_y*height-13);
    vertex(15, chA_y*height);
    vertex(0, chA_y*height+13);
  endShape();
	}
  
  // WaveWatch Display
  stroke(200,200,200,255);
  fill(200,200,200,150);
  rect(0,height-leftmargin,leftmargin,height); 
  stroke(20,20,20);
   fill(20,20,20,150);
  textAlign(CENTER);
  //text("T:"+time_resolution_str[time_res]+"\nA:"+amp_resolution_str[amp_res_A]+" B:"+amp_resolution_str[amp_res_B]+'\n',leftmargin/2,height-leftmargin+text_size+3);
  text("T:"+time_resolution_str[time_res],leftmargin/2,height-leftmargin+text_size+3);
  if (!trig_last) fill(100,100,100); else fill(0,100,0);
  if (!running) dstring="Stopped"; 
  if (running&&tmode==1) dstring="Ready";
  if (running&&tmode==0) dstring="Auto";  
  text(dstring,leftmargin/2,height-text_size+3);
  ellipse(10, height-text_size, 12, 12);  
  
  // CHA Display
  stroke(0,60,0);
  fill(0,60,0);
  textAlign(CENTER);
  text("ChA\n"+amp_resolution_str[amp_res_A],leftmargin/2,leftmargin/2+text_size/2);
  stroke(50,255,50);
  
    if (leftmenu==5*leftmargin) {
		fill(50,255,50,10);
		stroke(50,255,50,200);
        rect(1*leftmargin+3,3,leftmargin-3,leftmargin-6); 
		rect(2*leftmargin+3,3,leftmargin-3,leftmargin-6);
		rect(3*leftmargin+3,3,leftmargin-3,leftmargin-6);		
		if(measure_state>0) fill(50,255,50,100); else fill(50,255,50,10);
		rect(4*leftmargin+3,3,leftmargin-3,leftmargin-6);
		if(chA_active) fill(50,255,50,100); else fill(50,255,50,10);
		rect(5*leftmargin+3,3,leftmargin-3,leftmargin-6);
		stroke(50,255,50);
		textAlign(CENTER);
		text("Amp-",leftmargin+leftmargin/2,leftmargin/2+text_size/2);
		text("Amp+",2*leftmargin+leftmargin/2,leftmargin/2+text_size/2);
		text("<>",3*leftmargin+leftmargin/2,leftmargin/2+text_size/2); // Spare for calibration
		text("Meas",4*leftmargin+leftmargin/2,leftmargin/2+text_size/2);
		if (chA_active) {dstring="On";stroke(50,255,50);} else {dstring="Off";stroke(50,180,50);}
		text(dstring,5*leftmargin+leftmargin/2,leftmargin/2+text_size/2);	
  }
}

function display_xy_buffer(samplesA,samplesB,bufLen,color) {
  strokeWeight(sig_weight);
  stroke(color);
  fill(100,100,100,haloing); 
  beginShape();
  for (var i = 0; i < bufLen; i++){
    var yA = map(samplesA[i], -1, 1, -height/2, height/2);
	var yB = map(samplesB[i], -1, 1, -height/2, height/2);
    vertex(yA*gainA+width/2, yB*gainB+height/2);
  }
  endShape();
  noFill(); 	
}

// Raw Buffer Display
function display_raw_buffer(samples, bufLen,delta_y,gainl,color) {
  strokeWeight(sig_weight);
  stroke(color);
  fill(100,100,100,haloing); 
  beginShape();
  vertex(-1, delta_y*height);
  for (var i = 0; i < bufLen; i++){
    var x = map(i, 0, bufLen, xmin, xmax);
    //var y = map(samples[i], -1, 1, -height/2, height/2);
    vertex(x, -1*samples[i]*gainl + delta_y*height);
  }
  vertex(width+1, delta_y*height);
  endShape();
  noFill();  
}

function buffermon() {
	fill(255,255,255,80);
	stroke(255,255,255,80);
    if(running) rect(0,height-leftmargin,ia.mod(2400)*leftmargin/2400,-5); else rect(0,height-leftmargin,leftmargin,-5);
}

// draw debug text
function labelStuff(bufLen) {
  strokeWeight(1);
  stroke("red");
  textAlign(LEFT);

  text('LVL: '+ trig_lvl, leftmargin+10, height-2*leftmargin );
  text('POS: '+ trig_x +' TrigPos: '+trigpos, leftmargin+10, height-2*leftmargin+15);
  text('SR: '+ samplerate, leftmargin+10, height-2*leftmargin+30);
  text('INCA: '+ incA, leftmargin+10, height-2*leftmargin+45);
}

// draw Axis
function drawAxis() {
  strokeWeight(1.0);
  stroke(255,255,255,255);
  line(0,height/2,width,height/2);
  line(width/2,0,width/2,height); 
  for (var i = 1; i < 40; i = i+1) {
    line(width/2-width/300, height/2+20*i, width/2+width/300, height/2+20*i);
	line(width/2-width/300, height/2-20*i, width/2+width/300, height/2-20*i);
	line(width/2-20*i,height/2+height/300,width/2-20*i,height/2-height/300);
	line(width/2+20*i,height/2+height/300,width/2+20*i,height/2-height/300);
	if ( (i>0)&&(i.mod(1)==0) ) {
		if (i.mod(5)==0) stroke(255,255,255,60); else stroke(255,255,255,10);
		line(0,height/2-20*i,width,height/2-20*i);
		line(width/2-20*i,0,width/2-20*i,height); 
		line(0,height/2+i*20,width,height/2+i*20);
		line(width/2+i*20,0,width/2+i*20,height); 
		stroke(255,255,255,255);
	}
  }  
  
}

// called on Window resize
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  leftmargin=width*marginratio;  if (leftmargin>marginlimit) leftmargin=marginlimit;
  rightmargin=width*marginratio; if (rightmargin>marginlimit) rightmargin=marginlimit;
  scrspd = scrollspeed*width;
  xmin=width/2-dhsize;
  xmax=width/2+dhsize;
}

// =====================================
// ======== Interaction Subfunctions
// =====================================
function keyPressed() {
  if (key == ' ') {
	running=!running;
  }
}

function mouseClicked() {
  splashclick=false;
  if (measure_state>0) {
	switch(measure_state) {
			case 1: // First click
				m_points[0]=mouseX;
				m_points[1]=mouseY;
				measure_state=2;
			break;
			case 2: // Waiting for Measurement Second Click
				m_points[2]=mouseX;
				m_points[3]=mouseY;
				measure_state=3;
			break;
			case 3: // Display measurement Data
				//measure_state=0;
			break;
	}  
  }
  if (trig_set) { trig_update(); trig_set=false;}	
//Leftmenu Activation
  if (mouseX<leftmargin) {
	  if (mouseY>leftmargin&&mouseY<(height-leftmargin)) chA_y=mouseY/height; 
	  if (mouseY<leftmargin) {leftmenuactive = !leftmenuactive;  mainmenuactive=false;}
	  if (mouseY>(height-leftmargin)) {mainmenuactive=false; leftmenuactive=false; rightmenuactive=false; running=!running;}
  }
//Rightmenu Activation
  if (mouseX>(width-rightmargin)) { 
	  if ((mouseY<height-rightmargin)&&(mouseY>rightmargin))chB_y=mouseY/height; 
      if (mouseY>height-rightmargin) rightmenuactive = !rightmenuactive;
	  if (mouseY<rightmargin) {mainmenuactive = !mainmenuactive; leftmenuactive=false;}
  }
//Channel A Menu Clicks
  if (mouseY<leftmargin&&(leftmenu==5*leftmargin)) {
	  if ((mouseX>leftmargin)&&(mouseX<2*leftmargin)) {amp_res_A+=1; if(amp_res_A>=amp_resolution_str.length) amp_res_A-=1;amp_update();} //gainA += gainA*0.1;
	  if ((mouseX>2*leftmargin)&&(mouseX<3*leftmargin)) {amp_res_A-=1; if(amp_res_A<0) amp_res_A=0;amp_update();} //gainA -= gainA*0.1;
	  if ((mouseX>5*leftmargin)&&(mouseX<6*leftmargin)) chA_active = !chA_active;
	  if ((mouseX>4*leftmargin)&&(mouseX<5*leftmargin)) if (measure_state==0) {measure_state=1; mainmenuactive=false; leftmenuactive=false; rightmenuactive=false;} else measure_state=0;  
	 
  }
//Channel B Menu Clicks
  if ((mouseY>(height-rightmargin))&&(rightmenu==5*rightmargin)) {
	  if ((mouseX>(width-2*rightmargin))&&(mouseX<(width-1*rightmargin))) {amp_res_B+=1; if(amp_res_B>=amp_resolution_str.length) amp_res_B-=1;amp_update();} //gainB += gainB*0.1;
	  if ((mouseX>(width-3*rightmargin))&&(mouseX<(width-2*rightmargin))) {amp_res_B-=1; if(amp_res_B<0) amp_res_B=0;amp_update();}//gainB -= gainB*0.1;
	  if ((mouseX>(width-5*rightmargin))&&(mouseX<(width-4*rightmargin))) if (measure_state==0) {measure_state=1; mainmenuactive=false; leftmenuactive=false; rightmenuactive=false;} else measure_state=0;
	  if ((mouseX>(width-6*rightmargin))&&(mouseX<(width-5*rightmargin))) chB_active = !chB_active;
	 
  }
//Main Menu Clicks
  if (mouseY<leftmargin&&(mainmenu==6*leftmargin)) {
	  if ((mouseX>(width-2*rightmargin))&&(mouseX<(width-rightmargin))) {time_res+=1; if(time_res>=time_resolution.length) time_res=time_resolution.length-1;time_update(); measure_state=0;}
      if ((mouseX>(width-3*rightmargin))&&(mouseX<(width-2*rightmargin))) {time_res-=1; if(time_res<0) time_res=0;time_update();measure_state=0;}	 
      if ((mouseX>(width-4*rightmargin))&&(mouseX<(width-3*rightmargin))) {if(tsource==0) tsource=1; else tsource=0;}	  
      if ((mouseX>(width-5*rightmargin))&&(mouseX<(width-4*rightmargin))) {if(tmode==0) tmode=1; else {tmode=0; running=true;}}	  	  
	  if ((mouseX>(width-6*rightmargin))&&(mouseX<(width-5*rightmargin))) {trig_set=!trig_set; console.log(trig_set);}	  	  
	  if ((mouseX>(width-7*rightmargin))&&(mouseX<(width-6*rightmargin))) {trig_dir=!trig_dir; console.log(trig_dir);}	
	  }  
  

}

function mouseDragged() {
	if (mouseIsPressed) {
		if (trig_set) trig_update();
		if (mouseX<leftmargin&&mouseY>leftmargin&&mouseY<(height-leftmargin)) chA_y=mouseY/height;
		if ((mouseX>(width-rightmargin))&&(mouseY<(height-rightmargin))&&(mouseY>rightmargin)) chB_y=mouseY/height;
	}
}



