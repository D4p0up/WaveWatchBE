# WaveWatch Browser Edition

## A browser-powered Oscilloscope
Browser-based soundcard oscilloscope with a touch-compatible, screen-adaptive interface.

## Use it
Live demo, fully functional : https://www.bellesondes.fr/wavewatch

## Features
See project Home Page : https://www.bellesondes.fr/wavewatchbe

## Requirements
Web browser compatible with HTML5 audio API. Tested on Firefox and Chrome browsers.

To run it from your own website, it requires HTTPS/certificates enabled server site, because of HTML5 Audio API security restrictions.

## Version
Version 1.0 - Initial release